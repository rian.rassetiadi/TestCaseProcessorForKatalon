package database

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.Statement
import java.sql.Connection

import com.kms.katalon.core.annotation.Keyword


public class MyConnection {

	private static Connection connection = null;

	/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */

	//Establishing a connection to the DataBase

	@Keyword

	def connectDB(String database, String url, String dbname, String port, String username, String password){

		//Load driver class for your specific database type

		String conn = "jdbc:" +database+ "://" + url + ":" + port + "/" + dbname

		//Class.forName("org.sqlite.JDBC")

		//String connectionString = "jdbc:sqlite:" + dataFile

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = DriverManager.getConnection(conn, username, password)

		return connection

	}

	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned List of Map
	 */

	//Executing the constructed Query and Saving results in List of Map

	@Keyword

	def executeQuery(String queryString) {

		Statement stm = connection.createStatement()

		ResultSet rs = stm.executeQuery(queryString)
		
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		
		List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
		while (rs.next()){
			Map<String, Object> row = new HashMap<String, Object>(columns);
			for(int i = 1; i <= columns; ++i){
				row.put(md.getColumnName(i), rs.getObject(i));
			}
			rows.add(row);
		}
		
		return rows
	}

	//Closing the connection

	@Keyword

	def closeDatabaseConnection() {

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = null

	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */

	@Keyword

	def execute(String queryString) {

		Statement stm = connection.createStatement()

		boolean result = stm.execute(queryString)

		return result

	}

}