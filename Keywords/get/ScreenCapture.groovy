package get

import javax.imageio.ImageIO

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

import ru.yandex.qatools.ashot.*
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider
import ru.yandex.qatools.ashot.shooting.ShootingStrategies

public class Screencapture {

	KeywordLogger log = new KeywordLogger();

	@Keyword
	public String getElement(TestObject object, String fileName) {

		WebDriver driver = DriverFactory.getWebDriver();
		WebElement element = WebUiCommonHelper.findWebElement(object, 30);
		
		Screenshot screenshot;
		
		try{
			screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver,element);
		}catch(Exception e){
			screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, element);
		}
		
		String path = getImgPath(fileName);

		ImageIO.write(screenshot.getImage(), "JPG", new File(path))
		log.logPassed("Taking screenshot successfully\n[[ATTACHMENT|"+path+"]]")
		return "Hello World"
	}

	@Keyword
	public void getEntirePage(String fileName) {
		WebDriver driver = DriverFactory.getWebDriver();
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);

		String path = getImgPath(fileName);

		ImageIO.write(screenshot.getImage(), "JPG", new File(path));

		log.logPassed("Taking screenshot successfully\n[[ATTACHMENT|"+path+"]]");
	}

	@Keyword
	public void getEntirePageMobile(String fileName) {

		Mobile.takeScreenshot(getImgPath(fileName), FailureHandling.STOP_ON_FAILURE)
	}

	private String getImgPath(String fileName){

		String format = ".JPG"

		if(fileName.equals("")){
			fileName = new Date().getTime().toString() + format.toLowerCase();
		}else{
			if(!fileName.toLowerCase().contains(format.toLowerCase())){
				fileName = (fileName + format).toLowerCase()
			}else{
				fileName = fileName.toLowerCase()
			}
		}

		KeywordLogger logging = KeywordLogger.getInstance();
		String reportFolder = logging.getLogFolderPath();

		return reportFolder+File.separator+fileName;
	}
}
